/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_COMMON_NETLINK_H__
#define __MCASTD_COMMON_NETLINK_H__

#include <linux/types.h>
#include <linux/netlink.h>

#ifndef NETLINK_MCASTD
#define NETLINK_MCASTD 27
#endif

#define MCASTD_MAXNLMSGSIZE 4096
#define MCASTD_MAXNLMSGPAYLOAD (MCASTD_MAXNLMSGSIZE - NLMSG_LENGTH(0))

enum mcastdnl_group {
    MCASTDNL_GROUP_NONE,
    MCASTDNL_GROUP_AMX_ALWAYS,
    MCASTDNL_GROUP_AMX_OPTIONAL,
    MCASTDNL_GROUP_EXTRA_MIN,
    MCASTDNL_GROUP_EXTRA_MAX = 31,
    MCASTDNL_GROUP_COUNT
};

enum mcastdnl_type {
    MCASTDNL_TYPE_NEWOBJ = NLMSG_MIN_TYPE,
    MCASTDNL_TYPE_DELOBJ,
    MCASTDNL_TYPE_NOTIFY,
    MCASTDNL_TYPE_FCALL,
    MCASTDNL_TYPE_EXTRA_MIN,
    MCASTDNL_TYPE_EXTRA_MAX = NLMSG_MIN_TYPE + 31,
    MCASTDNL_TYPE_COUNT
};

#endif
