/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __MCASTD_COMMON_KVARIANT_H__
#define __MCASTD_COMMON_KVARIANT_H__

#include <linux/types.h>
#include <linux/netlink.h>

#define KVAR_LENGTH(var) (NLMSG_ALIGN(sizeof(struct kvarhdr)) + (var)->hdr.len)

enum kvartype {
    KVARTYPE_VOID,
    KVARTYPE_BOOL,
    KVARTYPE_INT,
    KVARTYPE_UINT,
    KVARTYPE_STRING,
    KVARTYPE_LIST,
    KVARTYPE_MAP,
    KVARTYPE_ADDR,
};

struct kvarhdr {
    __u16 type;
    __u16 len;
    __u16 cnt;
};

struct kvarlist {
    __u16 val;
};

struct kvarmap {
    __u16 key;
    __u16 val;
};

struct kvaraddr {
    __u8 fam;
    __u8 val[0];
};

struct kvar {
    struct kvarhdr hdr;
    union {
        int b;
        __s32 i;
        __u32 u;
        char s[0];
        struct kvarlist l[0];
        struct kvarmap m[0];
        struct kvaraddr a;
    } val;
};

struct kvarbuf {
    union {
        void* ptr;
        unsigned char* raw;
        struct kvar* var;
        struct nlmsghdr* nlh;
    } data;
    int size;
};

int kvar_validate(struct kvar* var);

int kvar_bool_val(struct kvar* var);

__s32 kvar_int_val(struct kvar* var);

__u32 kvar_uint_val(struct kvar* var);

char* kvar_string_val(struct kvar* var);

int kvar_list_count(struct kvar* var);

struct kvar* kvar_list_val(struct kvar* var, __u16 index);

int kvar_map_count(struct kvar* var);

char* kvar_map_key(struct kvar* var, __u16 index);

struct kvar* kvar_map_val(struct kvar* var, __u16 index);

struct kvar* kvar_map_find(struct kvar* var, const char* key);

__u8 kvar_addr_fam(struct kvar* var);

__u8* kvar_addr_val(struct kvar* var);

int kvar_void_set(struct kvarbuf* buf, struct kvar* var);

int kvar_bool_set(struct kvarbuf* buf, struct kvar* var, int b);

int kvar_int_set(struct kvarbuf* buf, struct kvar* var, __s32 i);

int kvar_uint_set(struct kvarbuf* buf, struct kvar* var, __u32 u);

int kvar_string_set(struct kvarbuf* buf, struct kvar* var, const char* s);

int kvar_list_init(struct kvarbuf* buf, struct kvar* var, __u16 cnt);

int kvar_list_edit(struct kvarbuf* buf, struct kvar* var, __u16 index,
                   struct kvar** val);

int kvar_list_done(struct kvarbuf* buf, struct kvar* var, __u16 index);

int kvar_map_init(struct kvarbuf* buf, struct kvar* var, __u16 cnt);

int kvar_map_edit(struct kvarbuf* buf, struct kvar* var, __u16 index,
                  const char* key, struct kvar** val);

int kvar_map_done(struct kvarbuf* buf, struct kvar* var, __u16 index);

int kvar_addr_set(struct kvarbuf* buf, struct kvar* var, __u8 fam, __u8* val);

#endif
