/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_dm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "../../include/mcastd/user/plugin.h"
#include "../../include/mcastd/common/netlink.h"

#include "../common/common.h"

#include "../mocks/mock_shell.h"
#include "../mocks/mock_netlink.h"

#include "test.h"

void test_optional_notifications(UNUSED void** state) {
    // 1 - Enable Optional Notification
    expect_value_count(__wrap_mcastdnl_join, group, MCASTDNL_GROUP_AMX_OPTIONAL, 1);
    amxut_dm_param_set(bool, "MCASTD.", "OptionalNotificationsEnable", true);
    amxut_bus_handle_events();

    // 2 - Disable Optional Notification
    expect_value_count(__wrap_mcastdnl_leave, group, MCASTDNL_GROUP_AMX_OPTIONAL, 1);
    amxut_dm_param_set(bool, "MCASTD.", "OptionalNotificationsEnable", false);
    amxut_bus_handle_events();
}

void test_snooping_routing_rules(UNUSED void** state) {
    // 1 - Disable IGMP Snooping (ipv4)
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "iptables -t mangle -D mcastd_pre -j SNOOP -p igmp -i LANDummyIntf -w", 1);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", false);
    amxut_bus_handle_events();

    // 2 - Enable MLD Snooping (ipv6)
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 2);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -A mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 130 -w", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -A mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 131 -w", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -A mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 132 -w", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -A mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 143 -w", 1);
    amxut_dm_param_set(cstring_t, "MCASTD.Intf.lan.", "Family", "ipv6");
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", true);
    amxut_bus_handle_events();

    // 3 - Disable MLD Snooping (ipv6)
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -D mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 130 -w", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -D mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 131 -w", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -D mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 132 -w", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ip6tables -t mangle -D mcastd_pre -j SNOOP -p icmpv6 -i LANDummyIntf --icmpv6-type 143 -w", 1);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", false);
    amxut_bus_handle_events();

    // 4 - Enable IGMP Snooping (ipv4)
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 2);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "iptables -t mangle -A mcastd_pre -j SNOOP -p igmp -i LANDummyIntf -w", 1);
    amxut_dm_param_set(cstring_t, "MCASTD.Intf.lan.", "Family", "ipv4");
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", true);
    amxut_bus_handle_events();
}

void test_snooping_l2_routing_rules(UNUSED void** state) {
    // 1 - Disable Snooping to delete iptables rules
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "iptables -t mangle -D mcastd_pre -j SNOOP -p igmp -i LANDummyIntf -w", 1);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", false);
    amxut_bus_handle_events();

    // 2 - Enable L2 Snooping
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 2);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -A PREROUTING -j SNOOP", 1);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", true);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "L2Snooping", true);
    amxut_bus_handle_events();

    // 3 - Disable Snooping
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -D PREROUTING -j SNOOP", 1);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", false);
    amxut_bus_handle_events();

    // 4 - Disable L2 Snooping & Enable Snooping
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 2);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "iptables -t mangle -A mcastd_pre -j SNOOP -p igmp -i LANDummyIntf -w", 1);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "SnoopingEnable", true);
    amxut_dm_param_set(bool, "MCASTD.Intf.lan.", "L2Snooping", false);
    amxut_bus_handle_events();
}


void test_snooping_exclusion_rules(UNUSED void** state) {
    // 1 - Remove port2 exclusion rule
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -D PREROUTING -i port1_Name -p 0x800 --ip-proto igmp -j DROP", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -D PREROUTING -i port2_Name -p 0x800 --ip-proto igmp -j DROP", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -I PREROUTING -i port1_Name -p 0x800 --ip-proto igmp -j DROP", 1);
    amxut_dm_param_set(csv_string_t, "MCASTD.Intf.lan.", "PortIGMPDisabled", "port1_Name");
    amxut_bus_handle_events();

    // 2 - Add back port2 exclusion rule
    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_NEWOBJ, 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -D PREROUTING -i port1_Name -p 0x800 --ip-proto igmp -j DROP", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -I PREROUTING -i port1_Name -p 0x800 --ip-proto igmp -j DROP", 1);
    expect_string_count(__wrap_mcastd_shprintf, cmd, "ebtables -t nat -I PREROUTING -i port2_Name -p 0x800 --ip-proto igmp -j DROP", 1);
    amxut_dm_param_set(csv_string_t, "MCASTD.Intf.lan.", "PortIGMPDisabled", "port1_Name,port2_Name");
    amxut_bus_handle_events();

}

void test_function_call(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "MCASTD");

    expect_value_count(__wrap_sendto, ((struct nlmsghdr*) buf)->nlmsg_type, MCASTDNL_TYPE_FCALL, 1);
    assert_int_equal(amxd_status_deferred, amxd_object_invoke_function(obj, "reconfigure", &args, &ret));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
