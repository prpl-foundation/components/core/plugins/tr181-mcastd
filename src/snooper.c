/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <mcastd/user.h>

#define ME "snooper"

typedef struct mcastdsn_intf {
    char* netdev_name;
    bool igmp_snooping;
    bool mld_snooping;
    amxc_var_t list_port_igmp_off;
} mcastdsn_intf_t;

static int mcastdsn_intf_dummy = 0;
static void* mcastdsn_intf_key = &mcastdsn_intf_dummy;

static int mcastdsn_rule_refcnt = 0;

static void mcastdsn_clearIGMPExclusionRules(amxc_var_t* ports) {
    amxc_var_for_each(port, ports) {
        SAH_TRACEZ_INFO(ME, "Clear ebtables rules for IGMP exclusion port : %s", GET_CHAR(port, NULL));
        mcastd_shprintf("ebtables -t nat -D PREROUTING -i %s -p 0x800 --ip-proto igmp -j DROP", GET_CHAR(port, NULL));
    }
}

static void mcastdsn_setIGMPExclusionRules(amxc_var_t* ports) {
    amxc_var_for_each(port, ports) {
        SAH_TRACEZ_INFO(ME, "Add ebtables rules for IGMP exclusion port : %s", GET_CHAR(port, NULL));
        mcastd_shprintf("ebtables -t nat -I PREROUTING -i %s -p 0x800 --ip-proto igmp -j DROP", GET_CHAR(port, NULL));
    }
}


static void mcastdsn_objectWriteHandler(amxd_object_t* object) {
    amxc_var_t list_port_igmp_off;
    mcastdsn_intf_t* intf = mcastd_getObjectUserData(object, mcastdsn_intf_key);
    int result = 0;

    amxc_var_init(&list_port_igmp_off);

    if(intf == NULL) {
        SAH_TRACEZ_INFO(ME, "Creation of mcastd_netmodel_t object");
        intf = calloc(1, sizeof(mcastdsn_intf_t));
        when_null_trace(intf, exit, ERROR, "Cannot allocate memory for mcastdsn_intf_t object")
        amxc_var_init(&intf->list_port_igmp_off);
        amxc_var_set_type(&intf->list_port_igmp_off, AMXC_VAR_ID_LIST);
        mcastd_addObjectUserData(object, mcastdsn_intf_key, intf);
    }

    const char* family = GET_CHAR(amxd_object_get_param_value(object, "Family"), NULL);
    bool snooping = amxd_object_get_value(bool, object, "SnoopingEnable", NULL);
    bool igmp_snooping = snooping && !strcmp(family? :"", "ipv4");
    bool mld_snooping = snooping && !strcmp(family? :"", "ipv6");
    bool l2snooping = amxd_object_get_value(bool, object, "L2Snooping", NULL);
    char* netdev_name = amxd_object_get_value(cstring_t, object, "NetDevName", NULL);
    char* portIGMP_disabled = amxd_object_get_value(cstring_t, object, "PortIGMPDisabled", NULL);

    amxc_var_set(csv_string_t, &list_port_igmp_off, portIGMP_disabled);
    amxc_var_cast(&list_port_igmp_off, AMXC_VAR_ID_LIST);

    amxc_var_compare(&list_port_igmp_off, &intf->list_port_igmp_off, &result);
    if(result != 0) {
        mcastdsn_clearIGMPExclusionRules(&intf->list_port_igmp_off);
        mcastdsn_setIGMPExclusionRules(&list_port_igmp_off);
        amxc_var_copy(&intf->list_port_igmp_off, &list_port_igmp_off);
    }

    if(!l2snooping) {
        if((igmp_snooping != intf->igmp_snooping) ||
           strcmp(netdev_name? :"", intf->netdev_name? :"")) {
            if(intf->igmp_snooping) {
                mcastd_shprintf("iptables -t mangle -D mcastd_pre -j SNOOP"
                                " -p igmp -i %s -w", intf->netdev_name);
                intf->igmp_snooping = false;
            }
            if(igmp_snooping && netdev_name && *netdev_name) {
                mcastd_shprintf("iptables -t mangle -A mcastd_pre -j SNOOP"
                                " -p igmp -i %s -w", netdev_name);
                intf->igmp_snooping = true;
            }
        }
        if((mld_snooping != intf->mld_snooping) ||
           strcmp(netdev_name? :"", intf->netdev_name? :"")) {
            if(intf->mld_snooping) {
                mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 130 -w", intf->netdev_name);
                mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 131 -w", intf->netdev_name);
                mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 132 -w", intf->netdev_name);
                mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 143 -w", intf->netdev_name);
                intf->mld_snooping = false;
            }
            if(mld_snooping && netdev_name && *netdev_name) {
                mcastd_shprintf("ip6tables -t mangle -A mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 130 -w", netdev_name);
                mcastd_shprintf("ip6tables -t mangle -A mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 131 -w", netdev_name);
                mcastd_shprintf("ip6tables -t mangle -A mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 132 -w", netdev_name);
                mcastd_shprintf("ip6tables -t mangle -A mcastd_pre -j SNOOP"
                                " -p icmpv6 -i %s --icmpv6-type 143 -w", netdev_name);
                intf->mld_snooping = true;
            }
        }
    } else {
        // l2snooping == true
        bool rule_exists = ( mcastdsn_rule_refcnt ? true : false);

        if(igmp_snooping != intf->igmp_snooping) {
            if(igmp_snooping) {
                mcastdsn_rule_refcnt++;
            } else {
                mcastdsn_rule_refcnt--;
            }
        }
        if(mld_snooping != intf->mld_snooping) {
            if(mld_snooping) {
                mcastdsn_rule_refcnt++;
            } else {
                mcastdsn_rule_refcnt--;
            }
        }

        if(rule_exists && (mcastdsn_rule_refcnt == 0)) {
            mcastd_shprintf("ebtables -t nat -D PREROUTING -j SNOOP");
        } else if(!rule_exists && (mcastdsn_rule_refcnt != 0)) {
            mcastd_shprintf("ebtables -t nat -A PREROUTING -j SNOOP");
        }

        intf->mld_snooping = mld_snooping;
        intf->igmp_snooping = igmp_snooping;
    } // l2snooping

    if(strcmp(netdev_name? :"", intf->netdev_name? :"")) {
        free(intf->netdev_name);
        intf->netdev_name = netdev_name;
    } else {
        free(netdev_name);
    }

    free(portIGMP_disabled);
exit:
    amxc_var_clean(&list_port_igmp_off);
}

static void mcastdsn_objectDelHandler(amxd_object_t* object) {
    mcastdsn_intf_t* intf = mcastd_getObjectUserData(object, mcastdsn_intf_key);
    if(!intf) {
        SAH_TRACEZ_ERROR(ME, "Failed to get mcastd_netmodel_t object");
        return;
    }

    mcastdsn_clearIGMPExclusionRules(&intf->list_port_igmp_off);

    bool l2snooping = amxd_object_get_value(bool, object, "L2Snooping", NULL);
    if(!l2snooping) {
        if(intf->igmp_snooping) {
            mcastd_shprintf("iptables -t mangle -D mcastd_pre -j SNOOP "
                            "-p igmp -i %s -w", intf->netdev_name);
        }
        if(intf->mld_snooping) {
            mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP "
                            "-p icmpv6 -i %s --icmpv6-type 130 -w", intf->netdev_name);
            mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP "
                            "-p icmpv6 -i %s --icmpv6-type 131 -w", intf->netdev_name);
            mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP "
                            "-p icmpv6 -i %s --icmpv6-type 132 -w", intf->netdev_name);
            mcastd_shprintf("ip6tables -t mangle -D mcastd_pre -j SNOOP "
                            "-p icmpv6 -i %s --icmpv6-type 143 -w", intf->netdev_name);
        }
    } else {
        // l2snooping == true
        bool rule_exists = ( mcastdsn_rule_refcnt ? true : false);

        if(intf->igmp_snooping || intf->mld_snooping) {
            mcastdsn_rule_refcnt--;
        }

        if(rule_exists && (mcastdsn_rule_refcnt == 0)) {
            mcastd_shprintf("ebtables -t nat -D PREROUTING -j SNOOP");
        }
    }

    mcastd_delObjectUserData(object, mcastdsn_intf_key);
    free(intf->netdev_name);
    amxc_var_clean(&intf->list_port_igmp_off);
    free(intf);
}

__attribute__((constructor))
static void snooping_init(void) {
    mcastd_addObjectWriteHandler("MCASTD.Intf.*", mcastdsn_objectWriteHandler);
    mcastd_addObjectDelHandler("MCASTD.Intf.*", mcastdsn_objectDelHandler);
}

__attribute__((destructor))
static void snooping_cleanup(void) {
    mcastd_delObjectWriteHandler("MCASTD.Intf.*", mcastdsn_objectWriteHandler);
    mcastd_delObjectDelHandler("MCASTD.Intf.*", mcastdsn_objectDelHandler);
    amxd_object_t* intf = amxd_dm_findf(get_dm(), "MCASTD.Intf");
    amxd_object_for_each(instance, it, intf) {
        amxd_object_t* object = amxc_container_of(it, amxd_object_t, it);
        mcastdsn_objectDelHandler(object);
    }
}
