/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>

#include <mcastd/common/kamx.h>
#include <mcastd/user/fcall.h>
#include <mcastd/user/kvariant.h>
#include <mcastd/user/netlink.h>

#define ME "fcall"

/* Info about function call on a object.
 *
 * - doneHandler: function the plugin must call when it has finished parsing the
 *      netlink reply message from the mcastd kernel module.
 *
 * - userdata: if not NULL, it points to a function_call_t instance, i.e. a AMX
 *      call. Each function call corresponds to a AMX function call. This
 *      plugin sets the userdata of the function_call_t instance to the
 *      pointer to the corresponding mcastd_fcall instance (with
 *      fcall_setUserData(..)). So the userdata of the fcall points to the
 *      corresponding AMX call, and vice versa. The plugin typically sends
 *      a netlink message to the kernel module if a AMX function is called
 *      on the data model. The plugin needs the info about the AMX
 *      function call to complete (reply to) the AMX function call when it
 *      receives a netlink reply message from the kernel module.
 *
 * - extraUserData: the plugin can register extra work to be done (in the
 *      doneHandler) when it receives a netlink reply message from the
 *      kernel module.
 */
struct mcastd_fcall {
    amxc_llist_it_t it;
    char* path;
    char* name;
    amxc_var_t args;
    amxc_var_t retval;
    mcastd_fcallHandler doneHandler;
    void* userdata;
    uint64_t callid;
    __u32 seq;
    mcastd_fcall_state_t state;
    int error;
};

static amxc_llist_t mcastd_fcalls = {NULL, NULL};

static void mcastd_fcall_destroy(mcastd_fcall_t* fcall) {
    amxc_llist_it_take(&fcall->it);
    free(fcall->path);
    free(fcall->name);
    amxc_var_clean(&fcall->args);
    amxc_var_clean(&fcall->retval);
    free(fcall);
}

static void mcastd_fcall_finish(mcastd_fcall_t* fcall, mcastd_fcall_state_t state) {
    fcall->state = state;
    if(fcall->doneHandler) {
        fcall->doneHandler(fcall);
    }
    mcastd_fcall_destroy(fcall);
}

/* Create and start executing a mcastd function call.
 *
 * The function starts executing the mcastd function call by sending a netlink
 * message with info about the function call (path, name, args) to the mcastd
 * kernel module.
 *
 * @param[in] userdata: typically a pointer to a function_call_t instance, i.e.
 *                      a AMX function call.
 *
 * @return the created mcastd_fcall_t object on success, NULL on error.
 */
mcastd_fcall_t* mcastd_fcall_create(const char* path, const char* name,
                                    amxc_var_t* args, amxc_var_t* retval,
                                    mcastd_fcallHandler doneHandler, void* userdata) {
    (void) retval;
    mcastd_fcall_t* fcall = calloc(1, sizeof(mcastd_fcall_t));
    if(!fcall) {
        SAH_TRACEZ_ERROR(ME, "calloc() failed");
        return NULL;
    }
    amxc_llist_append(&mcastd_fcalls, &fcall->it);
    fcall->path = strdup(path);
    fcall->name = strdup(name);
    if(!fcall->path || !fcall->name) {
        SAH_TRACEZ_ERROR(ME, "strdup() failed");
        goto error;
    }
    amxc_var_init(&fcall->args);
    amxc_var_init(&fcall->retval);
    fcall->doneHandler = doneHandler;
    fcall->userdata = userdata;
    fcall->state = mcastd_fcall_state_executing;

    struct nlmsghdr* nlh = mcastdnl_sendinit();
    struct kvarbuf buf = {
        .data = { .ptr = NLMSG_DATA(nlh) },
        .size = MCASTD_MAXNLMSGPAYLOAD
    };
    struct kvar* v0 = buf.data.var, * v1, * v2;
    kvar_list_init(&buf, v0, KAMX_FCALL_PROPERTY_COUNT);
    kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_PATH, &v1);
    kvar_string_set(&buf, v1, fcall->path);
    kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_PATH);
    kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_NAME, &v1);
    kvar_string_set(&buf, v1, fcall->name);
    kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_NAME);
    kvar_list_edit(&buf, v0, KAMX_FCALL_PROPERTY_ARGS, &v1);
    const amxc_htable_t* map = amxc_var_constcast(amxc_htable_t, args);
    kvar_map_init(&buf, v1, amxc_htable_size(map));
    int i = 0;
    amxc_var_for_each(arg, args) {
        kvar_map_edit(&buf, v1, i, amxc_var_key(arg), &v2);
        variant2kvar(&buf, v2, arg);
        kvar_map_done(&buf, v1, i);
        i++;
    }
    kvar_list_done(&buf, v0, KAMX_FCALL_PROPERTY_ARGS);

    mcastdnl_send(MCASTDNL_TYPE_FCALL, KVAR_LENGTH(v0));
    fcall->seq = nlh->nlmsg_seq;

    return fcall;
error:
    mcastd_fcall_destroy(fcall);
    return NULL;
}

void mcastd_fcall_cancel(mcastd_fcall_t* fcall) {
    mcastd_fcall_finish(fcall, mcastd_fcall_state_canceled);
}

mcastd_fcall_state_t mcastd_fcall_state(mcastd_fcall_t* fcall) {
    return fcall->state;
}

const char* mcastd_fcall_path(mcastd_fcall_t* fcall) {
    return fcall->path;
}

const char* mcastd_fcall_name(mcastd_fcall_t* fcall) {
    return fcall->name;
}

amxc_var_t* mcastd_fcall_args(mcastd_fcall_t* fcall) {
    return &fcall->args;
}

amxc_var_t* mcastd_fcall_retval(mcastd_fcall_t* fcall) {
    return &fcall->retval;
}

int mcastd_fcall_error(mcastd_fcall_t* fcall) {
    return fcall->error;
}

void* mcastd_fcall_userdata(mcastd_fcall_t* fcall) {
    return fcall->userdata;
}

uint64_t mcastd_fcall_callid(mcastd_fcall_t* fcall) {
    return fcall->callid;
}

uint64_t* mcastd_fcall_callidref(mcastd_fcall_t* fcall) {
    return &fcall->callid;
}

static mcastd_fcall_t* mcastd_fcall_find(__u32 seq) {
    mcastd_fcall_t* fcall;
    for(fcall = (mcastd_fcall_t*) amxc_llist_get_first(&mcastd_fcalls); fcall;
        fcall = (mcastd_fcall_t*) amxc_llist_it_get_next(&fcall->it)) {
        if(fcall->seq == seq) {
            break;
        }
    }
    if(!fcall) {
        SAH_TRACEZ_WARNING(ME, "No such fcall (seq=%u)", seq);
    }
    return fcall;
}

/* Process the netlink message.
 *
 * The function:
 * - looks for the corresponding mcastd_fcall_t object (using the netlink sequence
 *   number).
 * - extracts the return value, arguments and error indication from the netlink
 *   message, and assigns them respectively to the fields retval, args and error
 *   of the mcastd_fcall_t object.
 * - calls mcastd_fcall_finish(..).
 */
static int mcastd_fcall_input(struct nlmsghdr* nlh) {
    if(kamx_sanity_check(nlh)) {
        SAH_TRACEZ_WARNING(ME, "Received short or invalid message");
        return 0;
    }
    mcastd_fcall_t* fcall = mcastd_fcall_find(nlh->nlmsg_seq);
    if(!fcall) {
        return 0;
    }
    struct kvar* v0 = NLMSG_DATA(nlh), * v1;
    v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_RETVAL);
    kvar2variant(&fcall->retval, v1);
    v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_ARGS);
    int i;
    amxc_var_t arg;
    amxc_var_init(&arg);
    amxc_var_set_type(&fcall->args, AMXC_VAR_ID_HTABLE); // clean args
    for(i = 0; i < kvar_map_count(v1); i++) {
        kvar2variant(&arg, kvar_map_val(v1, i));
        amxc_var_t* tmp = amxc_var_add_new_key(&fcall->args, kvar_map_key(v1, i));
        amxc_var_move(tmp, &arg);
    }
    amxc_var_clean(&arg);
    v1 = kvar_list_val(v0, KAMX_FCALL_PROPERTY_ERROR);
    fcall->error = kvar_uint_val(v1);

    mcastd_fcall_finish(fcall, mcastd_fcall_state_done);
    return 0;
}

static void mcastd_fcall_printProperty(FILE* out, struct kvar* var, int index,
                                       const char* name) {
    struct kvar* val = kvar_list_val(var, index);
    if(!val) {
        return;
    }
    amxc_var_t variant;
    amxc_var_init(&variant);

    kvar2variant(&variant, val);
    fprintf(out, " %s=", name);
    amxc_var_dump_stream(&variant, out);
    amxc_var_clean(&variant);
}

static int mcastd_fcall_print(FILE* out, struct nlmsghdr* nlh) {
    fprintf(out, "FCALL ");
    if(kamx_sanity_check(nlh)) {
        fprintf(out, "<<short or invalid message>>");
        return 0;
    }
    fprintf(out, "{");
    struct kvar* v0 = NLMSG_DATA(nlh);
    mcastd_fcall_printProperty(out, v0, KAMX_FCALL_PROPERTY_PATH, "path");
    mcastd_fcall_printProperty(out, v0, KAMX_FCALL_PROPERTY_NAME, "name");
    mcastd_fcall_printProperty(out, v0, KAMX_FCALL_PROPERTY_RETVAL, "retval");
    mcastd_fcall_printProperty(out, v0, KAMX_FCALL_PROPERTY_ARGS, "args");
    mcastd_fcall_printProperty(out, v0, KAMX_FCALL_PROPERTY_ERROR, "error");
    fprintf(out, " }");
    return 0;
}

static int mcastd_fcall_err(int err, struct nlmsghdr* nlh) {
    mcastd_fcall_t* fcall = mcastd_fcall_find(nlh->nlmsg_seq);
    if(!fcall) {
        return -1;
    }
    fcall->error = err;
    mcastd_fcall_finish(fcall, mcastd_fcall_state_error);
    return -1;
}

__attribute__((constructor))
static void mcastd_fcall_init(void) {
    mcastdnl_register(MCASTDNL_TYPE_FCALL, mcastd_fcall_input,
                      mcastd_fcall_print, mcastd_fcall_err);
}

__attribute__((destructor))
static void mcastd_fcall_cleanup(void) {
    mcastdnl_unregister(MCASTDNL_TYPE_FCALL);
}

