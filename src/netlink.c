/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#include <debug/sahtrace.h>

#include <mcastd/user/netlink.h>

#define ME "netlink"

static int mcastdnl_sock = -1;
static __u32 mcastdnl_pid = 0;
static int mcastdnl_groups[MCASTDNL_GROUP_COUNT];

static struct {
    mcastdnl_input_cb input;
    mcastdnl_print_cb print;
    mcastdnl_err_cb err;
} mcastdnl_table[MCASTDNL_TYPE_COUNT - NLMSG_MIN_TYPE];

// TODO add ring buffer for error messages like in netdev

int mcastdnl_register(int msgtype, mcastdnl_input_cb input, mcastdnl_print_cb print,
                      mcastdnl_err_cb err) {
    if((msgtype < NLMSG_MIN_TYPE) || (msgtype >= MCASTDNL_TYPE_COUNT)) {
        return -EINVAL;
    }
    mcastdnl_table[msgtype - NLMSG_MIN_TYPE].input = input;
    mcastdnl_table[msgtype - NLMSG_MIN_TYPE].print = print;
    mcastdnl_table[msgtype - NLMSG_MIN_TYPE].err = err;
    return 0;
}

void mcastdnl_unregister(int msgtype) {
    if((msgtype < NLMSG_MIN_TYPE) || (msgtype >= MCASTDNL_TYPE_COUNT)) {
        return;
    }
    mcastdnl_table[msgtype - NLMSG_MIN_TYPE].input = NULL;
    mcastdnl_table[msgtype - NLMSG_MIN_TYPE].print = NULL;
    mcastdnl_table[msgtype - NLMSG_MIN_TYPE].err = NULL;
}

static const char* mcastdnl_print(struct nlmsghdr* nlh) {
#if 0
    printf("\n\n");
    int i, j;
    unsigned char* raw = (unsigned char*) nlh;
    int len = nlh->nlmsg_len;
    for(i = 0; i < len; i += 16) {
        printf("%04x: ", i);
        for(j = i; j < i + 16 && j < len; j++) {
            printf("%02x ", raw[j]);
        }
        for(; j < i + 16; j++) {
            printf("   ");
        }
        printf("| ");
        for(j = i; j < i + 16 && j < len; j++) {
            printf("%c", raw[j] < 32 ? '.' : raw[j]);
        }
        printf("\n");
    }
#endif

    static char msgbuf[512];
    if((nlh->nlmsg_type >= NLMSG_MIN_TYPE) &&
       (nlh->nlmsg_type < MCASTDNL_TYPE_COUNT) &&
       mcastdnl_table[nlh->nlmsg_type - NLMSG_MIN_TYPE].print) {
        memset(msgbuf, 0, sizeof(msgbuf));
        FILE* f = fmemopen(msgbuf, 512, "w");
        mcastdnl_table[nlh->nlmsg_type - NLMSG_MIN_TYPE].print(f, nlh);
        fclose(f);
    } else {
        sprintf(msgbuf, "{ msgtype=%u }", nlh->nlmsg_type);
    }

    return msgbuf;
}

int mcastdnl_fd(void) {
    return mcastdnl_sock;
}

union mcastdnl_buf {
    unsigned char raw[MCASTD_MAXNLMSGSIZE];
    struct nlmsghdr nlh;
};

static union mcastdnl_buf mcastdnl_sendbuf;
static union mcastdnl_buf mcastdnl_recvbuf;

struct nlmsghdr* mcastdnl_sendinit(void) {
    memset(&mcastdnl_sendbuf.nlh, 0, sizeof(struct nlmsghdr));
    mcastdnl_sendbuf.nlh.nlmsg_flags = NLM_F_REQUEST;
    memset(NLMSG_DATA(&mcastdnl_sendbuf.nlh), 0xbb, MCASTD_MAXNLMSGPAYLOAD);
    return &mcastdnl_sendbuf.nlh;
}

int mcastdnl_send(__u16 type, __u16 payload) {
    struct nlmsghdr* nlh = &mcastdnl_sendbuf.nlh;
    struct sockaddr_nl addr;
    memset(&addr, 0, sizeof(struct sockaddr_nl));
    addr.nl_family = AF_NETLINK;
    nlh->nlmsg_type = type;
    nlh->nlmsg_len = NLMSG_LENGTH(payload);
    nlh->nlmsg_seq = ({static int nlmsg_seq = 0; ++nlmsg_seq;});
    nlh->nlmsg_pid = mcastdnl_pid;
    SAH_TRACEZ_INFO(ME, "Send message #%lu: %s",
                    (unsigned long) nlh->nlmsg_seq,
                    mcastdnl_print(nlh));
    if(nlh->nlmsg_len > MCASTD_MAXNLMSGSIZE) {
        SAH_TRACEZ_ERROR(ME, "Message is too big -> reject");
        return -EINVAL;
    }
    int ret = sendto(mcastdnl_sock, nlh, nlh->nlmsg_len, 0,
                     (struct sockaddr*) &addr, sizeof(struct sockaddr_nl));
    if(ret < 0) {
        SAH_TRACEZ_ERROR(ME, "sendto() failed: %m");
    }
    return ret;
}

static __attribute__((noinline)) void mcastdnl_err(struct nlmsgerr* nlerr) {
    struct nlmsghdr* nlh = &nlerr->msg;
    if((nlh->nlmsg_type < NLMSG_MIN_TYPE) ||
       (nlh->nlmsg_type >= MCASTDNL_TYPE_COUNT)) {
        goto fallback;
    }
    int type_index = nlh->nlmsg_type - NLMSG_MIN_TYPE;
    if(!mcastdnl_table[type_index].err) {
        goto fallback;
    }
    if(!mcastdnl_table[type_index].err(-nlerr->error, nlh)) {
        return;
    }
fallback:
    SAH_TRACEZ_ERROR(ME, "Error %d (%s) received for request #%u: %s",
                     -nlerr->error, strerror(-nlerr->error),
                     nlh->nlmsg_seq, mcastdnl_print(nlh));
}

int mcastdnl_recv(int flags) {
    struct nlmsghdr* nlh = &mcastdnl_recvbuf.nlh;
    int ret = recv(mcastdnl_sock, nlh, MCASTD_MAXNLMSGSIZE, flags);
    if(ret < 0) {
        if((errno != EAGAIN) && (errno != EWOULDBLOCK) && (errno != EINTR)) {
            SAH_TRACEZ_ERROR(ME, "recv() failed: %m");
        }
        return ret;
    } else if(ret < (int) sizeof(struct nlmsghdr)) {
        SAH_TRACEZ_ERROR(ME, "recv() returned short message -> ignore");
        return ret;
    } else if(ret < (int) nlh->nlmsg_len) {
        SAH_TRACEZ_ERROR(ME, "recv() returned truncated message -> ignore");
        return ret;
    }
    switch(nlh->nlmsg_type) {
    case NLMSG_ERROR:
        mcastdnl_err(NLMSG_DATA(nlh));
        return ret;
    case NLMSG_NOOP:
        return ret;
    case NLMSG_DONE:
        return ret;
    default:
        break;
    }
    if((nlh->nlmsg_type < NLMSG_MIN_TYPE) ||
       (nlh->nlmsg_type >= MCASTDNL_TYPE_COUNT)) {
        SAH_TRACEZ_WARNING(ME, "Unsupported message type: %u",
                           nlh->nlmsg_type);
        return ret;
    }
    int type_index = nlh->nlmsg_type - NLMSG_MIN_TYPE;
    SAH_TRACEZ_INFO(ME, "Received message #%lu: %s",
                    (unsigned long) nlh->nlmsg_seq,
                    mcastdnl_print(nlh));
    if(!mcastdnl_table[type_index].input) {
        SAH_TRACEZ_WARNING(ME, "Unsupported message type: %u",
                           nlh->nlmsg_type);
        return ret;
    }
    mcastdnl_table[type_index].input(nlh);
    return ret;
}

static int mcastdnl_bind(void) {
    int i;
    int ret;
    struct sockaddr_nl addr;
    memset(&addr, 0, sizeof(struct sockaddr_nl));
    addr.nl_family = AF_NETLINK;
    addr.nl_pid = mcastdnl_pid;
    for(i = 1; i < MCASTDNL_GROUP_COUNT; i++) {
        if(mcastdnl_groups[i]) {
            addr.nl_groups |= 1 << (i - 1);
        }
    }
    ret = bind(mcastdnl_sock, (struct sockaddr*) &addr,
               sizeof(struct sockaddr_nl));
    if(ret < 0) {
        SAH_TRACEZ_ERROR(ME, "bind() failed: %m");
    }
    return ret;
}

void mcastdnl_join(int group) {
    if((group <= MCASTDNL_GROUP_NONE) || (group >= MCASTDNL_GROUP_COUNT)) {
        SAH_TRACEZ_ERROR(ME, "Invalid group Id: %d", group);
        return;
    }
    mcastdnl_groups[group]++;
    mcastdnl_bind();
}

void mcastdnl_leave(int group) {
    if((group <= MCASTDNL_GROUP_NONE) || (group >= MCASTDNL_GROUP_COUNT)) {
        SAH_TRACEZ_ERROR(ME, "Invalid group Id: %d", group);
        return;
    }
    if(mcastdnl_groups[group] <= 0) {
        return;
    }
    mcastdnl_groups[group]--;
    mcastdnl_bind();
}

__attribute__((constructor))
static void mcastd_netlink_init(void) {
    mcastdnl_pid = getpid();
    mcastdnl_sock = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_MCASTD);
    if(mcastdnl_sock == -1) {
        SAH_TRACEZ_ERROR(ME, "Could not connect to NETLINK_MCASTD");
    }
    memset(mcastdnl_groups, 0, sizeof(mcastdnl_groups));
    mcastdnl_bind();
}

__attribute__((destructor))
static void mcastd_netlink_cleanup(void) {
    if(mcastdnl_sock >= 0) {
        close(mcastdnl_sock);
    }
}
