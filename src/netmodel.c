/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <netmodel/client.h>

#include <mcastd/user.h>

/**
 * Trace zone
 *
 * Zone 'nemo' is already taken by lib_nemo-client. Use other name unique
 * within this plugin. (max length of trace zone name is 7)
 */
#define ME "netmodel"

#define close_query(Q) do { \
        if(Q != NULL) { \
            netmodel_closeQuery(Q); \
            Q = NULL; \
        } \
} while(0)

/**
 * Info per mcastd interface
 *
 * mcastd typically creates 4 instances of this type:
 * - 2 mcastd WAN interfaces: wanEnable=1:
 *   - 1 with name 'iptv' : family='ipv4'
 *   - 1 with name 'data6': family='ipv6'
 * - 2 mcastd LAN interfaces: wanEnable=0:
 *   - 1 with name 'lan' : family='ipv4'
 *   - 1 with name 'lan6': family='ipv6'
 *
 * Fields:
 * - name:
 *      Value of MCASTD.Intf.<kmcd_intf>.NetmodelIntf. Typical values:
 *      - iptv for the mcastd IPv4 WAN interface with name 'iptv'
 *      - data for the mcastd IPv6 WAN interface with name 'data'
        - lan for the mcastd WAN interfaces 'lan' and 'lan6'
 *      It's typically vveip0 for the mcastd WAN intf on a HGW with a PON intf
 *      and booted in bridged mode.
 * - family:
 *      Value of MCASTD.Intf.<kmcd_intf>.Family: ipv4 or ipv6.
 * - clientEnable:
 *      Value of MCASTD.Intf.<kmcd_intf>.ClientEnable.
 * - wanEnable:
 *      Value of MCASTD.Intf.<kmcd_intf>.WANEnable.
 * - name_query:
 *      netmodel query looking for netdev name. This name is typically 'bridge' for
 *      the mcastd LAN intfs, and 'vlan_iptv' for the mcastd WAN intfs. If the setup
 *      puts the IPTV WAN intf in a bridge called e.g. 'bridge_viptv', then
 *      this name is typically 'bridge_viptv'. It's typically vveip0 for the
 *      mcastd WAN intf on a HGW with a PON intf and booted in bridged mode.
 * - index_query:
 *      netmodel query looking for ifindex. This ifindex is typically the ifindex of
 *      the netdev intf found with 'name_query'. So it's typically the ifindex
 *      of an intf such as 'bridge' for the LAN intfs, and 'vlan_iptv' or
 *      'bridge_viptv' for the WAN intfs.
 * - intf_to_monitor_query:
 *      netmodel query to find the netmodel name of intf for which it should monitor the
 *      status. This query is only started on WAN intfs to monitor whether the
 *      the WAN intf is up or down.
 *      If QUERY_MCAST_IPTV_INTF is not defined, it looks for the IPv4 or IPv6
 *      intf in the traverse tree 'below' of the intf with name 'name' (e.g.
 *      'name' is typically 'iptv' for the mcastd IPv4 WAN intf ).
 *      If QUERY_MCAST_IPTV_INTF is defined, it looks for the netmodel name of the
 *      intf with flag mcast-iptv.
 * - intf_to_monitor_name:
 *      Result of intf_to_monitor_query.
 *      If QUERY_MCAST_IPTV_INTF is not defined, it's typically vlan_iptv.
 *      If QUERY_MCAST_IPTV_INTF is defined, it's typically vlan_iptv1 or
 *      vlan_iptv2.
 * - status_query:
 *      netmodel query to find out if the WAN intf is up or down. The query is
 *      dependent on whether clientEnable is true or false.
 *      1) If clientEnable is true, this plugin assumes that IPTV is routed.
 *      Then status_query runs query to check if intf_to_monitor_name is up or
 *      down. As explained above, the value of intf_to_monitor_name depends on
 *      whether QUERY_MCAST_IPTV_INTF is defined or not.
 *      1a) If QUERY_MCAST_IPTV_INTF is not defined, intf_to_monitor_name
 *      refers to an IPv4 or IPv6 intf. This is typically a VLAN intf. SaH
 *      typically doesn't set the flag netdev-monitor on VLAN intfs. So if a
 *      WAN intf goes down, the Status field of the VLAN intfs on top typically
 *      stays up. That's the reason this plugin starts a query to find the IPv4
 *      or IPv6 intf. status_query runs an isUp query on that IPv4 or IPv6 intf
 *      with traverse = 'down'.
 *      1b) If QUERY_MCAST_IPTV_INTF is defined, name_query typically returns
 *      'bridge_viptv'. But that bridge is typically always up. Therefore,
 *      intf_to_monitor_query looks for the intf with flag mcast-iptv in that
 *      bridge. And status_query runs an isUp query on that mcast-iptv intf.
 *      2) If clientEnable is false, then this plugin assumes IPTV is bridged,
 *      i.e. that the mcastd WAN intf is in a bridge. Then the plugin doesn't
 *      start a netmodel query to find an intf with flag ipv4, ipv6 or mcast-iptv.
 *      It starts a netmodel query directly on the netmodel intf given by 'name'. E.g.
 *      on a HGW with a PON intf and in bridged mode, this is typically vveip0.
 * - timer_delay_status_up: timer to delay setting Status to 1. If
 *     QUERY_MCAST_IPTV_INTF is defined (and if the HGW boots with
 *     clientEnable=1), Status refers to the intf with flag mcast-iptv. If the
 *     intf comes up, the HGW might add the intf to the bridge (typically
 *     bridge_viptv) immediately thereafter. This triggers a NETDEV_CHANGEUPPER
 *     event for the intf with flag mcast-iptv. If the BCM mcast driver receives
 *     such an event for an intf, it removes the MC acceleration rules having
 *     the intf as source or destination. To avoid that the driver removes rules
 *     which were just created, give the bridge with the IPTV intf some time to
 *     settle down.
 * - mcast_iptv_ifindex_query:
 *      netmodel query looking for ifindex of intf with flag mcast-iptv.
 */
typedef struct {
    char* name;
    char* family;
    bool clientEnable;
    bool wanEnable;
    netmodel_query_t* index_query;
    netmodel_query_t* name_query;
    netmodel_query_t* addr_query;
    netmodel_query_t* mtu_query;
    netmodel_query_t* intf_to_monitor_query;
    char* intf_to_monitor_name;
    amxp_timer_t* timer_restart_status_query;
    netmodel_query_t* status_query;
    netmodel_query_t* port_igmp_off_query;
#ifdef QUERY_MCAST_IPTV_INTF
    amxp_timer_t* timer_delay_status_up;
    netmodel_query_t* mcast_iptv_ifindex_query;
#endif
} mcastd_netmodel_t;

static int mcastd_netmodel_dummy = 0;
static void* mcastd_netmodel_key = &mcastd_netmodel_dummy;

static void mcastd_netmodel_indexHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    amxd_trans_t trans;
    when_true((result == NULL) || (result->type_id == AMXC_VAR_ID_NULL), exit);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(int32_t, &trans, "NetDevIndex", GET_INT32(result, NULL));
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
exit:
    return;
}

static void mcastd_netmodel_nameHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    amxd_trans_t trans;
    when_true((result == NULL) || (result->type_id == AMXC_VAR_ID_NULL), exit);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(cstring_t, &trans, "NetDevName", GET_CHAR(result, NULL));
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
exit:
    return;
}

static void mcastd_netmodel_addrHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    amxd_trans_t trans;
    when_true((result == NULL) || (result->type_id == AMXC_VAR_ID_NULL), exit);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(cstring_t, &trans, "Address", GET_CHAR(result, NULL));
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
exit:
    return;
}

static void mcastd_netmodel_mtuHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    amxd_trans_t trans;
    when_true((result == NULL) || (result->type_id == AMXC_VAR_ID_NULL), exit);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(int32_t, &trans, "MTU", GET_INT32(result, NULL));
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
exit:
    return;
}

static void mcastd_netmodel_portIGMPOffHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    amxc_var_t var;
    amxd_trans_t trans;

    when_true((result == NULL) || (result->type_id != AMXC_VAR_ID_HTABLE), exit);

    amxc_var_init(&var);
    amxc_var_convert(&var, result, AMXC_VAR_ID_LIST);
    amxc_var_cast(&var, AMXC_VAR_ID_CSV_STRING);

    SAH_TRACEZ_INFO(ME, "Set PortIGMPDisabled parameters to : %s", GET_CHAR(&var, NULL));

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(csv_string_t, &trans, "PortIGMPDisabled", GET_CHAR(&var, NULL));
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
    amxc_var_clean(&var);
exit:
    return;
}

#ifdef QUERY_MCAST_IPTV_INTF
static void handle_delay_status_up(pcb_timer_t* timer UNUSED, void* userdata) { // todo can't have pcb_timer_t
    amxd_object_t* intf = (amxd_object_t*) userdata;
    amxd_trans_t trans;
    mcastd_netmodel_t* nemo = mcastd_getObjectUserData(intf, mcastd_netmodel_key);
    if(nemo) {
        SAH_TRACEZ_INFO(ME, "%s.%s: set Status=1", nemo->name, nemo->family);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to get mcastd_netmodel_t object");
        SAH_TRACEZ_INFO(ME, "Set Status=1");
    }
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(bool, &trans, "Status", true);
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
}
#endif

static void mcastd_netmodel_statusHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    const bool up = GET_BOOL(result, NULL);
    amxd_trans_t trans;
    when_true((result == NULL) || (result->type_id == AMXC_VAR_ID_NULL), exit);
    SAH_TRACEZ_INFO(ME, "Updating status to %d", up);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(bool, &trans, "Status", up);
#ifdef QUERY_MCAST_IPTV_INTF
    mcastd_netmodel_t* nemo = mcastd_getObjectUserData(intf, mcastd_netmodel_key);
    if(!nemo) {
        SAH_TRACEZ_ERROR(ME, "Failed to get mcastd_netmodel_t object");
        amxd_trans_clean(&trans);
        return;
    }
    bool timer_started = false;
    if(up && nemo->clientEnable) {
        if(!nemo->timer_delay_status_up) {
            amxp_timer_new(&nemo->timer_delay_status_up, handle_delay_status_up, intf)
            if(nemo->timer_delay_status_up == NULL) {
                SAH_TRACEZ_ERROR(ME, "%s.%s: failed to create timer_delay_status_up",
                                 nemo->name, nemo->family);
            }
        }
        if(nemo->timer_delay_status_up) {
            SAH_TRACEZ_INFO(ME, "%s.%s: start timer to set Status to up in 4 s",
                            nemo->name, nemo->family);
            if(amxp_timer_start(nemo->timer_delay_status_up, 4000 /*ms*/)) {
                timer_started = true;
            } else {
                SAH_TRACEZ_ERROR(ME, "%s.%s: failed to start timer_delay_status_up",
                                 nemo->name, nemo->family);
            }
        }
    }
    if(!timer_started) {
        if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Transaction failed");
        }

        if(nemo && nemo->timer_delay_status_up) {
            amxp_timer_stop(nemo->timer_delay_status_up);
        }
    }
#else
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
#endif
    amxd_trans_clean(&trans);
exit:
    return;
}

#ifdef QUERY_MCAST_IPTV_INTF
/**
 * Plugin calls this function if ifindex of intf with flag 'mcast-iptv' changes.
 */
static void mcastd_netmodel_mcastIptvIfindexHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    amxd_trans_t trans;
    when_true((result == NULL) || (result->type_id == AMXC_VAR_ID_NULL), exit);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(int32_t, &trans, "NetDevIndexMcastIptv", GET_INT32(result, NULL));
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
exit:
    return;
}

#define IFINDEX_MCAST_IPTV_DONT_CARE -2

static void mcastd_netmodel_setMcastIptvIfindexToDontCare(amxd_object_t* intf) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, intf);
    amxd_trans_set_value(int32_t, &trans, "NetDevIndexMcastIptv", IFINDEX_MCAST_IPTV_DONT_CARE);
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
}
#endif /* QUERY_MCAST_IPTV_INTF */

/**
 * Plugin calls this function if netmodel name of intf changes for which it should
 * monitor the status.
 */
static void mcastd_netmodel_intfToMonitorHandler(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    const char* name = NULL;
    const char* name_old = NULL;
    mcastd_netmodel_t* nemo = NULL;

    nemo = mcastd_getObjectUserData(intf, mcastd_netmodel_key);
    when_null_trace(nemo, exit, ERROR, "Failed to get mcastd_netmodel_t object");

    name = GET_CHAR(result, NULL); // E.g. name = vlan_iptv, vlan_iptv1, ...
    when_null_trace(name, exit, ERROR, "%s.%s: failed to extract name", nemo->name, nemo->family);

    /* It's possible that strlen(name) is 0. */
    name_old = nemo->intf_to_monitor_name ? : "";
    if(strcmp(name_old, name)) {
        SAH_TRACEZ_INFO(ME, "%s.%s: intf_to_monitor_name: '%s' -> '%s'",
                        nemo->name, nemo->family, name_old, name);
        free(nemo->intf_to_monitor_name);
        nemo->intf_to_monitor_name = strdup(name);

        /**
         * todo, is this still needed?
         * Restart the netmodel query for the status of the intf. This involves calling
         * netmodel_closeQuery() for the old query. It's not allowed to call that
         * function from within a query handler. Therefore start a timer to restart
         * the query in the immediate future.
         */
        if(amxp_timer_start(nemo->timer_restart_status_query, /*timeout=*/ 0) != 0) {
            SAH_TRACEZ_WARNING(ME, "%s.%s: failed to start timer to query status of %s",
                               nemo->name, nemo->family, name);
        }
    }
exit:
    return;
}

/**
 * Restart netmodel query to monitor status of intf_to_monitor_name.
 *
 * The function:
 * - closes the existing query if there is one.
 * - starts a new query if intf_to_monitor_name is not empty.
 */
static void handle_restart_status_query(amxp_timer_t* timer UNUSED, void* userdata) {
    amxd_object_t* intf = (amxd_object_t*) userdata;
    mcastd_netmodel_t* nemo = NULL;

    nemo = mcastd_getObjectUserData(intf, mcastd_netmodel_key);
    when_null_trace(nemo, exit, ERROR, "Failed to get mcastd_netmodel_t object");
    if(nemo->status_query) {
        SAH_TRACEZ_INFO(ME, "%s.%s: close status_query", nemo->name, nemo->family);
        close_query(nemo->status_query);
    }
    when_null_trace(nemo->intf_to_monitor_name, exit, WARNING, "%s.%s: intf_to_monitor_name is NULL", nemo->name, nemo->family);
    if(strlen(nemo->intf_to_monitor_name) != 0) {
        SAH_TRACEZ_INFO(ME, "%s.%s: start querying status of %s", nemo->name, nemo->family,
                        nemo->intf_to_monitor_name);
        nemo->status_query = netmodel_openQuery_isUp(nemo->intf_to_monitor_name, "tr181-mcastd", NULL,
                                                     netmodel_traverse_down, mcastd_netmodel_statusHandler, intf);
    } else {
        /* If intf_to_monitor_name is empty, set Status to false */
        amxc_var_t status;
        amxc_var_init(&status);
        amxc_var_set(bool, &status, false);
        mcastd_netmodel_statusHandler(NULL, &status, intf);
        amxc_var_clean(&status);
    }
exit:
    return;
}

/**
 * Restart netmodel queries if one of the settings of the mcastd intf changes.
 *
 * @param[in] object: corresponds to MCASTD.Intf.<kmcd_intf> instance
 *
 * The netmodel queries collect info to be passed to kernel part of mcastd.
 */
static void mcastd_netmodel_writeIntf(amxd_object_t* object) {
    mcastd_netmodel_t* nemo = mcastd_getObjectUserData(object, mcastd_netmodel_key);
    if(!nemo) {
        SAH_TRACEZ_INFO(ME, "Creation of mcastd_netmodel_t object");
        nemo = calloc(1, sizeof(mcastd_netmodel_t));
        if(!nemo) {
            SAH_TRACEZ_ERROR(ME, "calloc failed");
            return;
        }
        mcastd_addObjectUserData(object, mcastd_netmodel_key, nemo);
    }
    char* name = amxd_object_get_value(cstring_t, object, "NetmodelIntf", NULL);
    char* family = amxd_object_get_value(cstring_t, object, "Family", NULL);
    bool clientEnable = amxd_object_get_value(bool, object, "ClientEnable", NULL);
    bool wanEnable = amxd_object_get_value(bool, object, "WANEnable", NULL);

    /* Return if none of the 4 previous fields changed value. */
    if(!strcmp(name? :"", nemo->name? :"") &&
       !strcmp(family? :"", nemo->family? :"") &&
       (clientEnable == nemo->clientEnable) &&
       (wanEnable == nemo->wanEnable   )) {
        free(name);
        free(family);
        return;
    }
    SAH_TRACEZ_INFO(ME, "name:'%s'->'%s', family='%s'->'%s', client=%d->%d, wan=%d->%d",
                    nemo->name? :"", name? :"", nemo->family? :"", family? :"",
                    nemo->clientEnable, clientEnable, nemo->wanEnable, wanEnable);
    /* flag     : Flag for all netmodel queries except the address query.
     *            Default set to value of family: 'ipv4' or 'ipv6'.
     * addrflag : Flag for the netmodel query for the IPv4 or IPv6 address
     */
    const char* flag = family;
    const char* addrflag = NULL;
    if(wanEnable && !clientEnable) {
        /* Upstream port for which the client (proxy) is disabled. The box then
         * runs in bridge mode, and the upstream port is part of the bridge.
         * Default this function searches for a netmodel interface with the flag
         * 'ipv4' or 'ipv6' flag set in the traverse tree 'down' of the netmodel
         * interface with name 'name'. But if the client (proxy) is disabled,
         * there is no need to search for an upstream IP interface. And there
         * might not even be an interface with the 'ipv4' or 'ipv6' flag set.
         * Instead look for an interface with the flags 'upstream-port' and 'enabled'.
         * In addition, only look for interfaces that are up (up).
         * This is needed to avoid selecting the wrong one when there are
         * multiple interfaces enabled with the upstream-port flag */
        flag = "upstream-port && enabled && up";
        /* Leave addrflag set to NULL to indicate this function should not do a
         * netmodel query to get the IPv4 or IPv6 address of the interface. */
    } else {
        if(!strcmp(family? :"", "ipv4")) {
            addrflag = "ipv4";
        } else {
            addrflag = "ipv6 && link";
        }
    }

    close_query(nemo->index_query);
    close_query(nemo->name_query);
    close_query(nemo->addr_query);
    close_query(nemo->mtu_query);
    close_query(nemo->intf_to_monitor_query);
    close_query(nemo->status_query);
    close_query(nemo->port_igmp_off_query);

    free(nemo->name);
    free(nemo->family);
    free(nemo->intf_to_monitor_name);

    nemo->name = name;
    nemo->family = family;
    nemo->wanEnable = wanEnable;
    nemo->clientEnable = clientEnable;
    nemo->intf_to_monitor_name = NULL;

#ifdef QUERY_MCAST_IPTV_INTF
    amxp_timer_delete(&nemo->timer_delay_status_up);
    close_query(nemo->mcast_iptv_ifindex_query);
#endif
    /**
     * Keep nemo->timer_restart_status_query (if it exists). But stop the timer
     * (if it's running). Its handler, handle_restart_status_query, uses
     * intf_to_monitor_name, and this function frees it and sets it to NULL.
     */
    if(nemo->timer_restart_status_query) {
        amxp_timer_stop(nemo->timer_restart_status_query);
    }

    SAH_TRACEZ_INFO(ME, "Open netmodel query for intf : %s", object->name);

    nemo->index_query = netmodel_openQuery_getFirstParameter(name, "tr181-mcastd",
                                                             "NetDevIndex", flag, NULL, mcastd_netmodel_indexHandler,
                                                             object);
    nemo->name_query = netmodel_openQuery_getFirstParameter(name, "tr181-mcastd",
                                                            "NetDevName", flag, NULL, mcastd_netmodel_nameHandler,
                                                            object);
    if(addrflag) {
        nemo->addr_query = netmodel_openQuery_luckyAddrAddress(name,
                                                               "tr181-mcastd", addrflag, NULL, mcastd_netmodel_addrHandler,
                                                               object);
    }
    nemo->mtu_query = netmodel_openQuery_getFirstParameter(name, "tr181-mcastd",
                                                           "MTU", flag, NULL, mcastd_netmodel_mtuHandler,
                                                           object);

    if(wanEnable) {
        /* The query checking if the interface is up must only run on the WAN IPTV interfaces. */
        if(clientEnable) {
            /**
             * Start netmodel query to find out for which interface this plugin should
             * monitor the status. That query is intf_to_monitor_query. Its
             * callback function uses the AMX timer timer_restart_status_query.
             */
            SAH_TRACEZ_INFO(ME, "WAN & Client ENABLED : Start netmodel query to find out which interface to monitor");
            if(!nemo->timer_restart_status_query) {
                SAH_TRACEZ_INFO(ME, "Create timer_restart_status_query");
                amxp_timer_new(&nemo->timer_restart_status_query, handle_restart_status_query, object);
                if(nemo->timer_restart_status_query == NULL) {
                    SAH_TRACEZ_ERROR(ME, "Failed to create timer_restart_status_query");
                }
            }

            if(nemo->timer_restart_status_query) {
#ifdef QUERY_MCAST_IPTV_INTF
                nemo->intf_to_monitor_query = netmodel_openQuery_getFirstParameter(name, "tr181-mcastd",
                                                                                   "Name", "mcast-iptv", NULL, mcastd_netmodel_intfToMonitorHandler, object);
#else
                nemo->intf_to_monitor_query = netmodel_openQuery_luckyIntf(name, "tr181-mcastd", flag,
                                                                           NULL, mcastd_netmodel_intfToMonitorHandler, object);
#endif
                if(!nemo->intf_to_monitor_query) {
                    SAH_TRACEZ_ERROR(ME, "%s.%s: failed to open query to find intf to monitor",
                                     name, family);
                }
            }
        } else {
            SAH_TRACEZ_INFO(ME, "WAN ENABLED & Client DISABLED : Query interface status");
            nemo->status_query = netmodel_openQuery_isUp(name, "tr181-mcastd", flag, NULL,
                                                         mcastd_netmodel_statusHandler, object);
        }
    } else {
        /* The query checking if bride ports have IGMP disabled must only run on the LAN interfaces. */
        nemo->port_igmp_off_query = netmodel_openQuery_getParameters(name, "tr181-mcastd", "Name", "no-igmp && inbridge", netmodel_traverse_down,
                                                                     mcastd_netmodel_portIGMPOffHandler, object);
    }

#ifdef QUERY_MCAST_IPTV_INTF
    /* The queries looking for the interface with the mcast-iptv flag must only run on the
     * WAN IPTV interfaces."
     * Only run the query if the HGW is in routed mode, i.e. if clientEnable is true.
     * If the HGW is in bridged mode, i.e. if clientEnable is false, do not run the
     * query and set NetDevIndexMcastIptv fixed to -2 to indicate the field is don't
     * care. The HGW only uses the mcast-iptv flag in routed mode.
     */
    if(wanEnable) {
        if(clientEnable) {
            nemo->mcast_iptv_ifindex_query = netmodel_openQuery_getFirstParameter(name, "tr181-mcastd",
                                                                                  "NetDevIndex", "mcast-iptv", NULL, mcastd_netmodel_mcastIptvIfindexHandler,
                                                                                  object);
        } else {
            mcastd_netmodel_setMcastIptvIfindexToDontCare(object);
        }
    }
#endif
}

static void mcastd_netmodel_delIntf(amxd_object_t* object) {
    mcastd_netmodel_t* nemo = NULL;

    nemo = mcastd_getObjectUserData(object, mcastd_netmodel_key);
    when_null_trace(nemo, exit, ERROR, "Failed to get mcastd_netmodel_t object")

    mcastd_delObjectUserData(object, mcastd_netmodel_key);
    close_query(nemo->index_query);
    close_query(nemo->name_query);
    close_query(nemo->addr_query);
    close_query(nemo->mtu_query);
    close_query(nemo->intf_to_monitor_query);
    close_query(nemo->port_igmp_off_query);
    free(nemo->intf_to_monitor_name);
    amxp_timer_delete(&nemo->timer_restart_status_query);
    close_query(nemo->status_query);
#ifdef QUERY_MCAST_IPTV_INTF
    amxp_timer_delete(&nemo->timer_delay_status_up);
    close_query(nemo->mcast_iptv_ifindex_query);
#endif
    free(nemo->name);
    free(nemo->family);
    free(nemo);
exit:
    return;
}

__attribute__((constructor))
static void mcastd_netmodel_init(void) {
    netmodel_initialize();
    mcastd_addObjectWriteHandler("MCASTD.Intf.*", mcastd_netmodel_writeIntf);
    mcastd_addObjectDelHandler("MCASTD.Intf.*", mcastd_netmodel_delIntf);
}

__attribute__((destructor))
static void mcastd_netmodel_cleanup(void) {
    mcastd_delObjectWriteHandler("MCASTD.Intf.*", mcastd_netmodel_writeIntf);
    mcastd_delObjectDelHandler("MCASTD.Intf.*", mcastd_netmodel_delIntf);
    amxd_object_t* intf = amxd_dm_findf(get_dm(), "MCASTD.Intf");
    amxd_object_for_each(instance, it, intf) {
        amxd_object_t* object = amxc_container_of(it, amxd_object_t, it);
        mcastd_netmodel_delIntf(object);
    }
    netmodel_cleanup();
}
