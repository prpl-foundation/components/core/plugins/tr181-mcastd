/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>
#include <sys/wait.h>
#include <signal.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <mcastd/common/kamx.h>
#include <mcastd/user/kvariant.h>
#include <mcastd/user/netlink.h>
#include <mcastd/user/plugin.h>
#include <mcastd/user/fcall.h>

#define ME "core"
#define MCASTD_LIB_DIR "/usr/lib/amx/mcastd-ext/"

static amxd_dm_t* dmref = NULL;
static amxo_parser_t* parserref = NULL;

typedef struct {
    amxc_llist_it_t it;
    const char* path;
    mcastd_objectHandler handle;
} mcastd_objh_t;
static amxc_llist_t mcastd_objhWrite = {NULL, NULL};
static amxc_llist_t mcastd_objhDel = {NULL, NULL};

typedef struct {
    amxc_llist_it_t it;
    const char* path;
    uint32_t type;
    mcastd_notifyHandler handle;
} mcastd_noth_t;
static amxc_llist_t mcastd_noth = {NULL, NULL};

typedef struct {
    amxc_llist_it_t it;
    void* key;
    void* data;
} mcastd_userdata_t;

typedef struct {
    amxc_llist_t userdata;
} mcastd_object_t;

typedef struct {
    amxc_llist_it_t it;
    void* handle;
} mcastd_plugin_t;
static amxc_llist_t mcastd_plugins = {NULL, NULL};

amxd_dm_t* get_dm(void) {
    return dmref;
}

static amxo_parser_t* get_parser(void) {
    return parserref;
}

static void mcastd_shprintf_array_it_free(amxc_array_it_t* it) {
    free(it->data);
}

/**
 * Run a command.
 *
 * @param[in] format: pass the command in a printf() like format (That's the
 *                    only reason why this function has 'printf' in its name).
 *
 * @attention The function logs an error if the exit status of the command is
 *            not 0. Hence you should only call this function for commands
 *            whose expected exit status is 0 upon success.
 */
void mcastd_shprintf(const char* format, ...) {
    int rv = -1;
    amxp_subproc_t* subproc = NULL;
    amxc_string_t cmd;
    char* cmd_buf = NULL;
    va_list args;
    amxc_llist_t cmd_list;
    amxc_array_t cmd_array;

    amxc_string_init(&cmd, 0);
    amxc_llist_init(&cmd_list);
    amxc_array_init(&cmd_array, 10);

    va_start(args, format);
    amxc_string_vsetf(&cmd, format, args);
    va_end(args);

    rv = amxc_string_split_to_llist(&cmd, &cmd_list, ' ');
    when_failed_trace(rv, exit, ERROR, "Failed to split command into list.");

    amxc_llist_iterate(it, &cmd_list) {
        amxc_string_t* word = amxc_container_of(it, amxc_string_t, it);
        amxc_array_append_data(&cmd_array, amxc_string_take_buffer(word));
    }

    rv = amxp_subproc_new(&subproc);
    when_failed_trace(rv, exit, ERROR, "Failed to create the subprocess.");

    rv = amxp_subproc_astart(subproc, &cmd_array);
    when_failed_trace(rv, exit, ERROR, "Failed to start the subprocess.");

    rv = amxp_subproc_wait(subproc, 1000);
    when_failed_trace(rv, exit, ERROR, "Failed to wait for the subprocess.");

    rv = amxp_subproc_get_exitstatus(subproc);
    when_failed_trace(rv, exit, ERROR, "Command returned exit code [%d]", rv);

exit:
    amxp_subproc_delete(&subproc);
    amxc_array_clean(&cmd_array, mcastd_shprintf_array_it_free);
    amxc_llist_clean(&cmd_list, amxc_string_list_it_free);
    amxc_string_clean(&cmd);
    free(cmd_buf);
}

static bool mcastd_pathMatch(const char* pattern, const char* path) {
    for(; *pattern || *path; pattern++, path++) {
        if(*pattern == '*') {
            while(*path && *path != '.') {
                path++;
            }
            path--;
        } else if(*pattern != *path) {
            return false;
        }
    }
    return true;
}

static void mcastd_addObjectHandler(amxc_llist_t* list,
                                    const char* path, mcastd_objectHandler handle) {
    mcastd_objh_t* objh = calloc(1, sizeof(mcastd_objh_t));
    when_null_trace(objh, exit, ERROR, "calloc() failed");
    objh->path = path;
    objh->handle = handle;
    amxc_llist_append(list, &objh->it);

exit:
    return;
}

static void mcastd_delObjectHandler(amxc_llist_t* list,
                                    const char* path, mcastd_objectHandler handle) {
    mcastd_objh_t* objh = NULL;
    for(objh = (mcastd_objh_t*) amxc_llist_get_first(list); objh;
        objh = (mcastd_objh_t*) amxc_llist_it_get_next(&objh->it)) {
        if(!strcmp(path, objh->path) && (handle == objh->handle)) {
            break;
        }
    }

    when_null_trace(objh, exit, WARNING, "Object handler not found at path %s", path);
    amxc_llist_it_take(&objh->it);

exit:
    free(objh);
}

void mcastd_addObjectWriteHandler(const char* path, mcastd_objectHandler handle) {
    mcastd_addObjectHandler(&mcastd_objhWrite, path, handle);
}

void mcastd_delObjectWriteHandler(const char* path, mcastd_objectHandler handle) {
    mcastd_delObjectHandler(&mcastd_objhWrite, path, handle);
}

void mcastd_addObjectDelHandler(const char* path, mcastd_objectHandler handle) {
    mcastd_addObjectHandler(&mcastd_objhDel, path, handle);
}

void mcastd_delObjectDelHandler(const char* path, mcastd_objectHandler handle) {
    mcastd_delObjectHandler(&mcastd_objhDel, path, handle);
}

void mcastd_addObjectUserData(amxd_object_t* object, void* key, void* data) {
    mcastd_userdata_t* userdata = NULL;
    mcastd_object_t* obj = (mcastd_object_t*) object->priv;
    if(obj == NULL) {
        obj = (mcastd_object_t*) calloc(1, sizeof(mcastd_object_t));
        when_null_trace(obj, exit, ERROR, "calloc() failed");
        object->priv = obj;
    }
    userdata = calloc(1, sizeof(mcastd_userdata_t));
    when_null_trace(userdata, exit, ERROR, "calloc() failed");
    userdata->key = key;
    userdata->data = data;
    amxc_llist_append(&obj->userdata, &userdata->it);

exit:
    return;
}

void mcastd_delObjectUserData(amxd_object_t* object, void* key) {
    mcastd_userdata_t* userdata = NULL;
    mcastd_object_t* obj = (mcastd_object_t*) object->priv;
    when_null_trace(obj, exit, WARNING, "Cannot get private data from object.");
    for(userdata = (mcastd_userdata_t*) amxc_llist_get_first(&obj->userdata);
        userdata;
        userdata = (mcastd_userdata_t*)
            amxc_llist_it_get_next(&userdata->it)) {
        if(userdata->key == key) {
            break;
        }
    }
    when_null_trace(userdata, exit, WARNING, "Userdata not found in private data.");
    amxc_llist_it_take(&userdata->it);

exit:
    free(userdata);
}

void* mcastd_getObjectUserData(amxd_object_t* object, void* key) {
    mcastd_userdata_t* userdata = NULL;
    void* data = NULL;
    mcastd_object_t* obj = (mcastd_object_t*) object->priv;
    when_null_trace(obj, exit, INFO, "Cannot get private data from object '%s'", object->name);
    for(userdata = (mcastd_userdata_t*) amxc_llist_get_first(&obj->userdata);
        userdata;
        userdata = (mcastd_userdata_t*)
            amxc_llist_it_get_next(&userdata->it)) {
        if(userdata->key == key) {
            data = userdata->data;
            break;
        }
    }
    when_null_trace(userdata, exit, INFO, "Userdata not found in private data.");

exit:
    return data;
}


static int mcastdnl_handleObj(struct nlmsghdr* nlh) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    if(kamx_sanity_check(nlh)) {
        SAH_TRACEZ_WARNING(ME, "Received short or invalid message");
        amxd_trans_clean(&trans);
        return 0;
    }
    struct kvar* v0 = NLMSG_DATA(nlh), * v1;
    v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PATH);
    char* path = kvar_string_val(v1);
    if(!path) {
        SAH_TRACEZ_WARNING(ME, "Object message has no path");
        amxd_trans_clean(&trans);
        return 0;
    }

    SAH_TRACEZ_INFO(ME, "Handling object with path %s and msg type %d", path, nlh->nlmsg_type);

    amxd_object_t* object;
    object = amxd_dm_findf(get_dm(), "%s", path);
    if(object || (nlh->nlmsg_type != MCASTDNL_TYPE_NEWOBJ)) {
        if(nlh->nlmsg_type == MCASTDNL_TYPE_NEWOBJ) {
            amxd_trans_select_object(&trans, object); // in case NEWOBJ
        }
        goto skip_create;
    }
    char* key = strrchr(path, '.');
    when_null(key, skip_create);
    *key++ = '\0';
    object = amxd_dm_findf(get_dm(), "%s", path);
    when_null(object, skip_create);
    amxd_trans_select_object(&trans, object);
    amxd_trans_add_inst(&trans, 0, key);
skip_create:
    when_null_trace(object, exit, WARNING, "Object %s not found", path);
    if(nlh->nlmsg_type == MCASTDNL_TYPE_DELOBJ) {
        amxd_trans_select_object(&trans, amxd_object_get_parent(object));
        amxd_trans_del_inst(&trans, 0, amxd_object_get_name(object, AMXD_OBJECT_NAMED));
        char* tmp = amxd_object_get_path(amxd_object_get_parent(object), AMXD_OBJECT_NAMED);
        free(tmp);
        goto skip_params;
    }
    v1 = kvar_list_val(v0, KAMX_OBJECT_PROPERTY_PARAMS);
    int i;
    amxc_var_t variant;
    amxc_var_init(&variant);
    for(i = 0; i < kvar_map_count(v1); i++) {
        kvar2variant(&variant, kvar_map_val(v1, i));
        amxd_trans_set_param(&trans, kvar_map_key(v1, i), &variant);
    }
    amxc_var_clean(&variant);

skip_params:
    status = amxd_trans_apply(&trans, get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed: %d", status);
    }
exit:
    amxd_trans_clean(&trans);
    return 0;
}

static int mcastdnl_handleNotify(struct nlmsghdr* nlh) {
    if(kamx_sanity_check(nlh)) {
        SAH_TRACEZ_WARNING(ME, "Received short or invalid message");
        return 0;
    }
    // build notification
    struct kvar* v0 = NLMSG_DATA(nlh), * v1;
    v1 = kvar_list_val(v0, KAMX_NOTIFY_PROPERTY_TYPE);
    uint32_t type = kvar_uint_val(v1);
    v1 = kvar_list_val(v0, KAMX_NOTIFY_PROPERTY_PATH);
    char* path = kvar_string_val(v1);
    v1 = kvar_list_val(v0, KAMX_NOTIFY_PROPERTY_NAME);
    char* name = kvar_string_val(v1);
    if(!type) {
        SAH_TRACEZ_WARNING(ME, "Notification has no type");
        return 0;
    }

    SAH_TRACEZ_INFO(ME, "Handling notify with path %s", path);

    amxc_var_t notification;
    amxc_var_init(&notification);
    amxc_var_set_type(&notification, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &notification, "type", type);
    amxc_var_add_key(cstring_t, &notification, "path", path);
    amxc_var_add_key(cstring_t, &notification, "name", name);
    v1 = kvar_list_val(v0, KAMX_NOTIFY_PROPERTY_PARAMS);
    int i;
    amxc_var_t* par = amxc_var_add_key(amxc_htable_t, &notification, "parameters", NULL);
    amxc_var_t variant;
    amxc_var_init(&variant);
    for(i = 0; i < kvar_map_count(v1); i++) {
        kvar2variant(amxc_var_add_new_key(par, kvar_map_key(v1, i)), kvar_map_val(v1, i));
    }
    amxc_var_clean(&variant);

    // distribute to local subscribers
    mcastd_noth_t* noth;
    for(noth = (mcastd_noth_t*) amxc_llist_get_first(&mcastd_noth); noth;
        noth = (mcastd_noth_t*) amxc_llist_it_get_next(&noth->it)) {
        if(!mcastd_pathMatch(noth->path, path? :"")) {
            continue;
        }
        if(noth->type != type) {
            continue;
        }
        noth->handle(&notification);
    }

    // distribute to remote subscribers
    amxd_object_t* object = amxd_dm_findf(get_dm(), "MCASTD");
    amxd_object_send_signal(object, "mcastd", &notification, false);
    amxc_var_clean(&notification);

    return 0;
}

static int mcastdnl_print(FILE* out, struct nlmsghdr* nlh) {
    switch(nlh->nlmsg_type) {
    case MCASTDNL_TYPE_NEWOBJ: fprintf(out, "NEWOBJ "); break;
    case MCASTDNL_TYPE_DELOBJ: fprintf(out, "DELOBJ "); break;
    case MCASTDNL_TYPE_NOTIFY: fprintf(out, "NOTIFY "); break;
    default:
        fprintf(out, "UNKNOWN<%d> ", nlh->nlmsg_type);
        break;
    }
    if(kamx_sanity_check(nlh)) {
        fprintf(out, "<<short or invalid message>>");
        return 0;
    }
    amxc_var_t variant;
    amxc_var_init(&variant);
    kvar2variant(&variant, NLMSG_DATA(nlh));
    amxc_var_dump_stream(&variant, out);
    amxc_var_clean(&variant);
    return 0;
}

static void pushObject(amxd_object_t* object, __u16 type) {
    struct kvarbuf buf = {
        .data = { .ptr = NLMSG_DATA(mcastdnl_sendinit()) },
        .size = MCASTD_MAXNLMSGPAYLOAD
    };
    struct kvar* v0 = buf.data.var, * v1, * v2;
    kvar_list_init(&buf, v0, KAMX_OBJECT_PROPERTY_COUNT);
    char* path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PATH, &v1);
    kvar_string_set(&buf, v1, path? :"");
    free(path);
    path = NULL;
    kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PATH);
    if(type == MCASTDNL_TYPE_DELOBJ) {
        goto skip_params;
    }

    amxc_var_t params;
    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_private);
    const amxc_htable_t* map = amxc_var_constcast(amxc_htable_t, &params);
    int i = 0, paramcount = amxc_htable_size(map);
    kvar_list_edit(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS, &v1);
    kvar_map_init(&buf, v1, paramcount);
    amxc_var_for_each(parameter, &params) {
        kvar_map_edit(&buf, v1, i,
                      amxc_var_key(parameter), &v2);
        variant2kvar(&buf, v2, parameter);
        kvar_map_done(&buf, v1, i);
        i++;
    }
    kvar_list_done(&buf, v0, KAMX_OBJECT_PROPERTY_PARAMS);
    amxc_var_clean(&params);

skip_params:
    mcastdnl_send(type, KVAR_LENGTH(v0));
}

static void handleObject(amxd_object_t* object, amxc_llist_t* list) {
    mcastd_objh_t* h = NULL;
    char* path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    when_null_trace(path, exit, ERROR, "Cannot get object path.");
    for(h = (mcastd_objh_t*) amxc_llist_get_first(list); h;
        h = (mcastd_objh_t*) amxc_llist_it_get_next(&h->it)) {
        if(!mcastd_pathMatch(h->path, path? :"")) {
            continue;
        }
        h->handle(object);
    }
exit:
    free(path);
}

static amxd_status_t mcastd_writeObject(amxd_object_t* object,
                                        UNUSED amxd_param_t* const param,
                                        UNUSED amxd_action_t reason,
                                        UNUSED const amxc_var_t* const args,
                                        UNUSED amxc_var_t* const retval,
                                        UNUSED void* priv) {
    pushObject(object, MCASTDNL_TYPE_NEWOBJ);
    handleObject(object, &mcastd_objhWrite);
    return amxd_status_ok;
}

static amxd_status_t mcastd_delObject(amxd_object_t* object,
                                      UNUSED amxd_param_t* const param,
                                      UNUSED amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    pushObject(object, MCASTDNL_TYPE_DELOBJ);
    handleObject(object, &mcastd_objhDel);
    mcastd_object_t* obj = (mcastd_object_t*) object->priv;
    if(obj != NULL) {
        when_false_trace(amxc_llist_is_empty(&obj->userdata), exit, ERROR, "Somebody forgot to clean up his mess!");
    }
    status = amxd_status_ok;
exit:
    free(obj);
    return status;
}

static void mcastd_defaultDone(mcastd_fcall_t* fcall) {
    when_true_trace(mcastd_fcall_state(fcall) == mcastd_fcall_state_canceled, exit, INFO, "fcall canceled");
    amxd_status_t status = (mcastd_fcall_error(fcall) != 0) ? amxd_status_unknown_error : amxd_status_ok;
    amxd_function_deferred_done(mcastd_fcall_callid(fcall), status, NULL, mcastd_fcall_retval(fcall));
exit:
    return;
}

static void mcastd_defaultCancel(UNUSED uint64_t call_id, void* userdata) {
    mcastd_fcall_t* fcall = (mcastd_fcall_t*) userdata;

    mcastd_fcall_cancel(fcall);
}

static amxd_status_t mcastd_defaultExec(amxd_object_t* object,
                                        amxd_function_t* func,
                                        amxc_var_t* args,
                                        amxc_var_t* retval) {
    amxd_status_t status = amxd_status_unknown_error;
    char* path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    when_null_trace(path, exit, ERROR, "Cannot get object path.");
    mcastd_fcall_t* fcall = mcastd_fcall_create(path,
                                                amxd_function_get_name(func),
                                                args, retval, mcastd_defaultDone, NULL);
    when_null_trace(fcall, exit, ERROR, "mcastd_fcall_create() failed.");
    status = amxd_function_defer(func, mcastd_fcall_callidref(fcall), retval, mcastd_defaultCancel, fcall);
    when_failed_trace(status, exit, ERROR, "amxd_function_defer() failed.");
    status = amxd_status_deferred;

exit:
    free(path);
    return status;
}

static void mcastd_reconfigure(amxd_object_t* object, bool handle_object) {
    pushObject(object, MCASTDNL_TYPE_NEWOBJ);
    if(handle_object) {
        SAH_TRACEZ_INFO(ME, "Reconfigure handling object %s", object->name);
        handleObject(object, &mcastd_objhWrite);
    }

    amxd_object_for_each(child, it, object) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        mcastd_reconfigure(obj, handle_object);
    }
    amxd_object_for_each(instance, it, object) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        mcastd_reconfigure(obj, handle_object);
    }
}

static void mcastd_loadPlugin(const char* name) {
    char* path = NULL;
    mcastd_plugin_t* plugin = NULL;

    int r = asprintf(&path, MCASTD_LIB_DIR "%s", name);
    when_true_trace(r == -1, exit, ERROR, "asprintf() failed with path : "MCASTD_LIB_DIR "%s", name)
    if(strstr(name, ".so")) {
        plugin = calloc(1, sizeof(mcastd_plugin_t));
        when_null_trace(plugin, exit, ERROR, "Calloc failed")
        plugin->handle = dlopen(path, RTLD_NOW);
        if(!plugin->handle) {
            const char* err = dlerror();
            (void) err;
            SAH_TRACEZ_ERROR(ME, "Could not open %s: %s", path, err);
            free(plugin);
            goto exit;
        }
        amxc_llist_append(&mcastd_plugins, &plugin->it);
        SAH_TRACEZ_INFO(ME, "Plugin %s loaded", name);
    }
exit:
    free(path);
}

static void mcastd_setDefaultHandlers(amxd_object_t* object) {
    amxd_object_add_action_cb(object, action_object_destroy, mcastd_delObject, NULL);
    amxd_object_for_each(function, it, object) {
        amxd_function_t* func = amxc_container_of(it, amxd_function_t, it);
        if(func->impl == NULL) {
            amxd_function_set_impl(func, mcastd_defaultExec);
        }
    }
    amxd_object_for_each(instance, it, object) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        mcastd_setDefaultHandlers(obj);
    }
    amxd_object_for_each(child, it, object) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        mcastd_setDefaultHandlers(obj);
    }
}

amxd_status_t _reconfigure(amxd_object_t* object,
                           UNUSED amxd_function_t* func,
                           UNUSED amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    mcastd_reconfigure(object, false);
    return amxd_status_ok;
}

static void enable_optional_notifications(bool join) {
    if(join) {
        mcastdnl_join(MCASTDNL_GROUP_AMX_OPTIONAL);
        SAH_TRACEZ_INFO(ME, "Joined MCASTDNL_GROUP_AMX_OPTIONAL");
    } else {
        mcastdnl_leave(MCASTDNL_GROUP_AMX_OPTIONAL);
        SAH_TRACEZ_INFO(ME, "Leaved MCASTDNL_GROUP_AMX_OPTIONAL");
    }
}

void _opt_notif_enable_changed(UNUSED const char* const sig_name,
                               const amxc_var_t* const event_data,
                               UNUSED void* const priv) {
    enable_optional_notifications(GETP_BOOL(event_data, "parameters.OptionalNotificationsEnable.to"));
}

void _tuner_changed(UNUSED const char* const sig_name,
                    const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(get_dm(), event_data);
    when_null_trace(object, exit, ERROR, "Cannot get object from event.");
    mcastd_writeObject(object, NULL, action_invalid, NULL, NULL, NULL);
exit:
    return;
}

void _intf_changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const event_data,
                   UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(get_dm(), event_data);
    when_null_trace(object, exit, ERROR, "Cannot get object from event.");
    mcastd_writeObject(object, NULL, action_invalid, NULL, NULL, NULL);
exit:
    return;
}

static void mcastdnl_peerRead(UNUSED int fd, UNUSED void* data) {
    mcastdnl_recv(0);
}

static bool plugin_initialize(void) {
    struct dirent* dirent = NULL;
    DIR* dir = NULL;
    amxd_object_t* root = NULL;
    bool ret = false;
    int rv = -1;

    mcastdnl_join(MCASTDNL_GROUP_AMX_ALWAYS);

    root = amxd_dm_findf(get_dm(), "MCASTD");
    when_null_trace(root, exit, ERROR, "Cannot get root object.");

    // load extensions
    dir = opendir(MCASTD_LIB_DIR);
    if(dir != NULL) {
        while((dirent = readdir(dir))) {
            mcastd_loadPlugin(dirent->d_name);
        }
        closedir(dir);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to open" MCASTD_LIB_DIR);
    }

    // set default handlers
    mcastd_setDefaultHandlers(root);

    rv = amxo_parser_parse_string(get_parser(), "?include '${odl.directory}/${name}.odl':'${odl.dm-defaults}';", amxd_dm_get_root(get_dm()));
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Parsing Error: %s\n", amxo_parser_get_message(get_parser()));
    }

    enable_optional_notifications(amxd_object_get_value(bool, root, "OptionalNotificationsEnable", NULL));
    mcastd_reconfigure(root, true);

    // register netlink callbacks
    mcastdnl_register(MCASTDNL_TYPE_NEWOBJ, mcastdnl_handleObj,
                      mcastdnl_print, NULL);
    mcastdnl_register(MCASTDNL_TYPE_DELOBJ, mcastdnl_handleObj,
                      mcastdnl_print, NULL);
    mcastdnl_register(MCASTDNL_TYPE_NOTIFY, mcastdnl_handleNotify,
                      mcastdnl_print, NULL);

    rv = amxp_connection_add(mcastdnl_fd(), mcastdnl_peerRead, NULL, AMXO_CUSTOM, NULL);
    when_failed_trace(rv, exit, ERROR, "Cannot add file descriptor.");
    ret = true;
exit:
    return ret;
}

static void plugin_cleanup(void) {
    while(!amxc_llist_is_empty(&mcastd_plugins)) {
        mcastd_plugin_t* plugin = (mcastd_plugin_t*) amxc_llist_get_last(&mcastd_plugins);
        amxc_llist_it_take(&plugin->it);
        dlclose(plugin->handle);
        free(plugin);
    }

    mcastdnl_unregister(MCASTDNL_TYPE_NEWOBJ);
    mcastdnl_unregister(MCASTDNL_TYPE_DELOBJ);
    mcastdnl_unregister(MCASTDNL_TYPE_NOTIFY);

    amxp_connection_remove(mcastdnl_fd());
}

int _main(int reason,
          amxd_dm_t* dm,
          amxo_parser_t* parser) {
    switch(reason) {
    case 0:     // START
        dmref = dm;
        parserref = parser;
        plugin_initialize();
        break;
    case 1:     // STOP
        plugin_cleanup();
        dmref = NULL;
        parserref = NULL;
        break;
    default:
        break;
    }

    return 0;
}

