# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.7 - 2024-09-10(07:13:32 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.1.6 - 2024-07-05(10:05:45 +0000)

### Other

- amx plugin should not run as root user

## Release v0.1.5 - 2024-05-23(19:51:03 +0000)

### Fixes

- Deprecated use of _init constructor

## Release v0.1.4 - 2024-05-14(07:58:40 +0000)

### Other

- Add support for multicast with MLDv2 (IPv6)

## Release v0.1.3 - 2024-05-06(07:09:04 +0000)

### Other

- Disable IGMP per bridge port

## Release v0.1.2 - 2024-04-24(16:31:56 +0000)

### Other

- Fixed module init

## Release v0.1.1 - 2024-04-23(14:25:21 +0000)

### Other

- Port multicast proxy plugin (KMCD) to amx

## Release v0.1.0 - 2023-12-05(11:26:48 +0000)

### New

- port multicast proxy plugin to amx

### Other

- Opensource component
- Rename plugin to tr181-mcastd

