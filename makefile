include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C src/libxt_snoop all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C src/libxt_snoop clean
	$(MAKE) -C odl clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/libmcastd.so $(DEST)/usr/lib/amx/$(COMPONENT)/libmcastd.so
	$(INSTALL) -d -m 0755 $(DEST)/usr/lib
	ln -sfr $(DEST)/usr/lib/amx/$(COMPONENT)/libmcastd.so $(DEST)/usr/lib/libmcastd.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/core.so $(DEST)/usr/lib/amx/mcastd-ext/core.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(DEST)/$(INCLUDEDIR)/mcastd/user
	$(INSTALL) -D -p -m 0644 include/mcastd/user/*.h $(DEST)$(INCLUDEDIR)/mcastd/user/
	$(INSTALL) -D -p -m 0644 include/mcastd/user.h $(DEST)$(INCLUDEDIR)/mcastd/user.h
	$(INSTALL) -D -p -m 0644 odl/tr181-mcastd.odl $(DEST)/etc/amx/$(COMPONENT)/tr181-mcastd.odl
	$(INSTALL) -D -p -m 0644 odl/mcastd_definition.odl $(DEST)/etc/amx/$(COMPONENT)/mcastd_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.d/;)
	$(INSTALL) -D -p -m 0644 config/mcastd.firewall $(DEST)/etc/amx/tr181-firewall/rules4/mcastd.firewall
	$(INSTALL) -D -p -m 0644 config/mcastd_ipv6.firewall $(DEST)/etc/amx/tr181-firewall/rules6/mcastd_ipv6.firewall
	$(INSTALL) -D -p -m 0755 scripts/mcastd.sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 src/libxt_snoop/libxt_SNOOP.so $(DEST)$(LIBDIR)/iptables/libxt_SNOOP.so

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/libmcastd.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/libmcastd.so
	$(INSTALL) -d -m 0755 $(PKGDIR)/usr/lib
	rm -f $(PKGDIR)/usr/lib/libmcastd.so
	ln -sfr $(PKGDIR)/usr/lib/amx/$(COMPONENT)/libmcastd.so $(PKGDIR)/usr/lib/libmcastd.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/core.so $(PKGDIR)/usr/lib/amx/mcastd-ext/core.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(INCLUDEDIR)/mcastd/user
	$(INSTALL) -D -p -m 0644 include/mcastd/user/*.h $(PKGDIR)$(INCLUDEDIR)/mcastd/user/
	$(INSTALL) -D -p -m 0644 include/mcastd/user.h $(PKGDIR)$(INCLUDEDIR)/mcastd/user.h
	$(INSTALL) -D -p -m 0644 odl/tr181-mcastd.odl $(PKGDIR)/etc/amx/$(COMPONENT)/tr181-mcastd.odl
	$(INSTALL) -D -p -m 0644 odl/mcastd_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/mcastd_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -D -p -m 0644 config/mcastd.firewall $(PKGDIR)/etc/amx/tr181-firewall/rules4/mcastd.firewall
	$(INSTALL) -D -p -m 0644 config/mcastd_ipv6.firewall $(PKGDIR)/etc/amx/tr181-firewall/rules6/mcastd_ipv6.firewall
	$(INSTALL) -D -p -m 0755 scripts/mcastd.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 src/libxt_snoop/libxt_SNOOP.so $(PKGDIR)$(LIBDIR)/iptables/libxt_SNOOP.so
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/mcastd_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test