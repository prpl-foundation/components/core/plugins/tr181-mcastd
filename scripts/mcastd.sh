#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}

name="tr181-mcastd"

# Run scripts starting with 'S' in subfolder of /etc/amx/tr181-mcastd/.
#
# Arguments:
# - name of subfolder. Examples: init.d, debuginfo.
# - target. Examples: start, stop, debuginfo.
# - verbose: 0 or 1. If 1, it logs a message for each script it runs.
run_scripts() {
	local folder
	local target
	local verbose
	folder=$1
	target=$2
	verbose=$3
	if [ -d /etc/amx/tr181-mcastd/$folder ]; then
		for script in /etc/amx/tr181-mcastd/$folder/S*; do
			[ ! -f $script ] && continue
			if [ $verbose -eq 1 ]; then
				echo "** $script $target"
			fi
			sh $script $target
		done
	fi
}

case $1 in
	start|boot)
		modprobe mcastd-core
		modprobe mcastd-proxy
		run_scripts init.d start 0

		tr181-mcastd -D
		;;
	stop|shutdown)
		if [ -f /var/run/${name}.pid ]; then
			kill `cat /var/run/${name}.pid`
		else
			killall ${name}
		fi
		# give the plugin time to shutdown properly.
		sleep 1

		run_scripts init.d stop 0
		rmmod mcastd-proxy
		# Sometimes tr181-mcastd is not completely stopped and still depends
		# on mcastd-core. Then modprobe command below to remove mcastd_core fails.
		# Wait 1 second if refcnt for mcastd_core is not 0.
		core_info=$(grep mcastd-core /proc/modules)
		if [ -n "$core_info" ]; then
			ref_cnt=$(echo $core_info | cut -d" " -f3)
			if [ $ref_cnt -ne 0 ]; then
				sleep 1
			fi
		fi
		rmmod mcastd-core
		;;
	debuginfo)
		ubus-cli "MCASTD.?"
		# If GET_DEBUG is 1, this script is called from getDebugInformation. Then
		# do not dump stuff which is already dumped by that script.
		if [ "$GET_DEBUG" != "1" ]; then
			show_cmd grep mcastd /web/version.txt
			echo "=============================================="
			# If mcastd processes an IGMP/MLD report, it queries the FDB (Forwarding
			# Database) of the bridge on which it is snooping to get the port of
			# the bridge on which the report arrived. Dump the content of that
			# database.
			show_cmd ip -B n
		fi
		mcastd_intfs=$(ubus-cli "MCASTD.Intf?0" | tail -n +2)
		for intf in $mcastd_intfs; do
			snoopingEnable=$(pcb_cli "$intf.SnoopingEnable?" | tr "=" "\n" | tail -n 1)
			if [ $snoopingEnable -eq 1 ]; then
				echo "=============================================="
				echo "[$(date)]: ubus-cli $intf.getGroups()"
				ubus-cli "$intf.getGroups()"
			fi
		done
		run_scripts debuginfo debuginfo 1
		;;

	restart)
		$0 stop
		$0 start
		;;
	log)
		echo "TODO"
		;;
	*)
		echo "Usage: $0 [start|stop|debuginfo|restart|log]"
		;;
esac
